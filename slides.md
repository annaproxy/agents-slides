---
author: Anna
title: Agents
subtitle: En andere dingen over de studie KI
institute: 
theme: metropolis
mainfont: Open Sans
mainfontoptions: Scale=0.8
sansfont: Open Sans
sansfontoptions: Scale=0.8
monofont: Droid Sans Mono
monofontoptions: Scale=0.8
header-includes: |
     \usepackage[dutch]{babel}
     `\newcommand{\alerttwo}[1]{{\color{uuPurple}#1}}`{=latex}
---

## Welkom

Agents & More!

![](img/begin.png)

## Inhoud

\tableofcontents

# De Agents track

## De Agents track

* Hoe kunnen we computers slimme dingen laten doen?

    * Ze kennis geven en alles laten uitrekenen
    
    * Ze data geven en zelf de kennis laten leren


## De Agents track

**Vakken**:

* Kunstmatige Intelligentie

* Intelligente Systemen

* Computationele Intelligentie

* Machine Learning

## De Agents track

* \alerttwo{Kunstmatige Intelligentie:}

    * Overzicht van allerlei technieken en ideeen

    * Van zoekproblemen tot reinforcement learning tot neuraal netwerk

    * Code in python 
    
* Intelligente Systemen

* Computationele Intelligentie

* Machine Learning

---

![Vind de kortste weg in een doolhof](img/doolhof.png)

---

![Voorspel de meest waarschijnlijke uitkomst met een Bayesiaans netwerk](img/bayes.png)

## De Agents track

* Kunstmatige Intelligentie:

* \alerttwo{Intelligente Systemen}

    * Dieper in logica, automatisch bewijzen
    
    * Kennisrepresentatie
    
    * Code in prolog 

* Computationele Intelligentie

* Machine Learning

---

![Logisch programmeren, zoals beschreven door docent Tomas Klos](img/logica.png)

## De Agents track

* Kunstmatige Intelligentie

* Intelligente Systemen

* \alerttwo{Computationele Intelligentie}

    * Zoekproblemen, CSPs
    
    * Evolutionaire algoritmen 
    
    * Code in C#

* Machine Learning

---

![Geef elke knoop een kleur, zorg dat knopen met een pijl ertussen niet dezelfde kleur hebben](img/graaf.png)

## De Agents track

* Kunstmatige Intelligentie:

* Intelligente Systemen

* Computationele Intelligentie

* Machine Learning

    * Computers kunnen leren van data
    
    * Voorspel nieuwe data
    
    * Code in Python
    
--- 

![Classificatie: Gegeven twee eigenschappen x,y, voorspel wat een hond of een kat is](img/classificatie_ml.png)

---

![Regressie: Gegeven x, voorspel wat y is](img/regressie_ml.png)

## De Agents track

* Programmeren 

* Puzzelen 

* \alert{Overlap?} 
    

# Andere vakken!

## Schrijf je in voor vakken!

Meer uitdaging? Meer leren? 

* Meer vakken doen is makkelijk te regelen

. . . 

* Volg een minor

    * Minor informatica: volg 4 vakken van informatica
    
    * Minor wiskunde: volg 4 vakken van wiskunde
    
## Schrijf je in voor vakken!
    
* Zorg alleen dat...

    * Geen fysieke overlap (timeslots)
    
    * Geen inhoudelijke overlap (Zie [`Students -> KI -> Profileringsruimte`](https://students.uu.nl/gw/ki/studieprogramma/profileringsruimte))
    
![Timeslots](img/weekklein.jpg){ width=250px }

. . . 
    
* Hoe vind ik vakken?

    * Informaticavakken: [`www.cs.uu.nl/education` ](http://www.cs.uu.nl/education)

    * Alle vakken: [`Osiris -> Onderwijs` ](https://osiris.uu.nl/osiris_student_uuprd/Onderwijs.do)
    * Om te kijken wat studenten van vorige jaren vonden: [`caracal.uu.nl`](https://caracal.uu.nl/) 


---

![Education](img/education.png)

[`www.cs.uu.nl/education` ](http://www.cs.uu.nl/education/)

---

![Osiris](img/osiris.png)

[`Osiris -> Onderwijs` ](https://osiris.uu.nl/osiris_student_uuprd/Onderwijs.do)

## Wat vakken uit mijn curriculum

KI-gerelateerde vakken in \alerttwo{paars}:

* Functioneel programmeren

* \alerttwo{Beeldverwerking}

* \alerttwo{Talen en compilers}

* Webtechnologie

* Databases

* \alerttwo{Data-analyse en retrieval}

* ...

# Scriptie

## Mijn scriptie

\alerttwo{Machine Learning!}

Dyslexie voorspellen aan de hand van vocabulaire data. 

---

![Data: Hoeveel woorden kennen kinderen van rond de 2?](img/data.png)

---

![Classificatie: Hoe goed voorspelt mijn model de dyslexie-status? (50% is random)](img/classificatie.png)

---

![Regressie: Hoe oud voorspelt mijn model dat de kinderen zijn](img/regressie.png)

# Master

## Masteropleiding kiezen

* Artificial Intelligence

    * AI in Utrecht

    * AI in Amsterdam
    
    * AI in een andere stad: Nijmegen, Groningen...

. . . 

* Iets anders?

    * Computing Science in Utrecht

    * Logic in Amsterdam


# Vragen?

## Deze slides:

[`gitlab.com/annaproxy/agents-slides`](https://www.gitlab.com/annaproxy/agents-slides)

(Download `slides.pdf` of bekijk `slides.md` voor optimaal gebruik (linkjes klikken enzo)










